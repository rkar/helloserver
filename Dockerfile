FROM golang:1.10 as builder
WORKDIR /go/src/helloserver
COPY *.go .
RUN go get github.com/gorilla/websocket
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo helloserver.go

FROM alpine:3.7
RUN apk add --no-cache curl
WORKDIR /
COPY --from=builder /go/src/helloserver/helloserver .

ENTRYPOINT ["/helloserver"]
