package main

import (
  "net/http"
  "strings"
  "strconv"
  "fmt"
  "os"
  "github.com/gorilla/websocket"
)

const DEFAULT_PORT = 8080

var isHealthy = true
var isReady = false

/* ---
 * Handler function for the root request.
 * The function processes health message and hcf requests and otherwise sends
 * a personalized hello page.
 */
func sendHello(w http.ResponseWriter, r *http.Request) {
  fmt.Printf("Req: %s %s\n", r.Host, r.URL.Path)

  name := strings.TrimPrefix(r.URL.Path, "/")

  if strings.EqualFold("ishealthy", name) {
    // Health check
    if isHealthy {
      isReady = true
      w.Write([]byte("Healthy"))
    } else {
      isReady = false
      http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
    }
  } else if strings.EqualFold("hcf", name) {
    // Halt and catch fire
    isHealthy = false
    isReady = false
    w.Write([]byte("Halt and catch fire ..."))
  } else if strings.EqualFold("isready", name) {
    // Readiness check
    if isReady {
      w.Write([]byte("Ready"))
    } else {
      http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
    }
  } else {
    hostname, err := os.Hostname()
    if err != nil {
      isReady = false
      panic(err)
    } else {
      isReady = true
    }
    message := getHelloPage(name, hostname)
    w.Write([]byte(message))
  }
}

/* ---
 * Handler function for web socket requests.
 */
func webSocketHandler(w http.ResponseWriter, r *http.Request) {
  conn, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
  if err != nil {
    http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
  } else {
    fmt.Printf("WsReq: %s %s\n", r.Host, r.URL.Path)
  }
  go pumpWebSocketMessage(conn)
}

/* ---
 * Web socket message loop.
 * Reads a message and writes a message. After that the connection
 * is being closed.
 */
func pumpWebSocketMessage(conn *websocket.Conn) {
  defer conn.Close()
  for {
    _, p, err := conn.ReadMessage()
    if err != nil {
      fmt.Println("Error reading message:", err)
      break
    }

    if string(p) == "getstate" {
      if err = conn.WriteMessage(websocket.TextMessage, []byte("WS ok")); err != nil {
        fmt.Println(err)
        break
      }
    }
  }
}

/* ---
 * Returns the string representing the HTML page with the hello message.
 */
func getHelloPage(userName, hostName string) string {
  return fmt.Sprintf(
`<html>
  <body>
    <p>Hello %s.<br>You are on host: %s</p>
    <p>Websocket state: <span id="wsstate">UNKNOWN</span></p>
    <p>Websocket message: <span id="wsmsg">UNKNOWN</span></p>
    <script>
      const $wsstate = document.getElementById("wsstate");
      const $wsmsg = document.getElementById("wsmsg");
      const baseHost = location.host;
      const protocol = (location.protocol === "https:") ? "wss:" : "ws:";
      const wsUrl = protocol + "//" + baseHost + "/ws";
      console.log("Trying WS connect:", wsUrl);
      const ws = new WebSocket(wsUrl);
      ws.onopen = e => {
        $wsstate.innerText = "CONNECTED";
        ws.send("getstate");
        setInterval(_ => {
          // Use this in order to keep the websocket connection open
          ws.send("ping");
        },3000);
      }
      ws.onerror = e => {
        $wsstate.innerText = "ERROR";
      }
      ws.onmessage = e => {
        $wsmsg.innerText = e.data;
      }
      ws.onclose = e => {
        $wsstate.innerText = "CLOSED";
      }
    </script>
  </body>
</html>`, userName, hostName)
}

/* ---
 * Main function to set up the handler functions and start the server listening.
 */
func main() {
  port := DEFAULT_PORT
  if len(os.Args) > 1 {
    var err error
    port, err = strconv.Atoi(os.Args[1])
    if err != nil {
      fmt.Printf("<%s> cannot be interpreted as port number. Using default.\n", os.Args[1])
      port = DEFAULT_PORT
    }
  }

  fmt.Printf("Now listening on port %d (use /hcf to catch fire)\n", port)

  http.HandleFunc("/", sendHello)
  http.HandleFunc("/ws", webSocketHandler)
  if err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil); err != nil {
    panic(err)
  }
}
