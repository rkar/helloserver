# HelloServer

Sample server project to play around with Docker and Kubernetes.
The server returns a hello message for the name given in the URL.
When everything is running simply open http://localhost in your browser.

## Create the Docker image

The server is build statically for a Linux OS. This happens as part of the
docker build process. The server is deployed as a Docker image (based on
alpine with curl added):

    docker build -t hello-server .

In order to remove the remaining intermediate container from the multi stage
build you may also call:

    docker system prune

## Deploy in Kubernetes

    kubectl create -f hello-server.yaml

# NGiNX Ingress controller installation

The application relies on an [NGiNX ingress controller](https://github.com/nginxinc/kubernetes-ingress).
The controller can be installed and started by calling:

    kubectl apply -f install-kubernetes-ingress.yaml

The controller can be removed from the Kubernetes cluster by deleting
the namespace:

    kubectl delete namespace nginx-ingress

## Create a self-signed certificate (needed for SSL connections) (optional)

A simple self-signed certificate (and private key) can be created by calling:

    openssl req -nodes -x509 -new -keyout self_signed.key -out self_signed.cert -days 1000 -subj '/CN=localhost'

This will create two files called self\_signed.cert (the certificate) and
self\_signed.key (the private key). the content of both files needs to be base64
encoded in order to use them as config mal values for secrets in YAML:

    cat self_signed.cert | base64
    cat self_signed.key | base64

The YAML files already contain a self-signed certificate. This section acts only
as knowledge store in case a certificate needs to be created.
